# App Code Hint

É um aplicativo desenvolvido para facilitar acesso a comandos e tarefas rotineiras no sistema.
Com o app você consegue:
- Ter ajuda de código;
- Campos editáveis;
- Organização e filtro de linhas escritas em documentos;
- Acesso a automação de mouse e teclado;
- Print Screen sem precisar salvar a imagem no computador.

Aplicativo standalone desenvolvido em React e Electron.

Possui as bibliotecas:
**robojs**
**iohook**

## Utilização

Para abrir o app use a hotkey CMD+; no MAC e Ctrl+; no windows.
O arquivo `hints.xml` é onde você deve editar os comandos.

Como o aplicativo não tem icone nem barra, para fechar o aplicativo você deve abrí-lo com o hotkey do seu sistema e finalizá-lo com `CMD+q`no mac ou `ALT+f4` no windows.

### Instalação

Tive uns problemas em usar o yarn para instalar, porém uso ele para rodar o aplicativo. Recomendo a instalação da repo com `npm install` e após a instalação, execute o `yarn rebuild` para compilar algumas dependencias extras e depois é só executar com `yarn electron:serve`.

### Build

<!-- prettier-ignore -->
No aplicativo compilado, você precisa mover o arquivo `hints.xml` para dentro dele. Aperte o botão direito e ponha `Mostrar conteúdo do pacote`, em seguida copie o arquivo para `Contents\Resources`.


### Link para o app compilado para MAC
[Code Hint](https://cloud.lipe.ph/$/zZEz8)
