const { app, BrowserWindow, ipcMain, globalShortcut, clipboard, nativeImage } = require('electron')
const ioHook = require('iohook');// Listener para clique de mouse e teclado

const path = require('path')
const isDev = require('electron-is-dev')
const robot = require("robotjs"); // Automatizar mouse, teclado e print
let Jimp = require('jimp'); // Manipular imagem

require('@electron/remote/main').initialize()

var win = null;
var printArea = null;
var mouseDownPos = null
var mouseUpPos= null

function createWindow() {
  // Cria a interface do app React
 
   win = new BrowserWindow({
    width: process.platform === 'darwin'?1000:800,
    height: 250,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      contextIsolation: false,
      preload: __dirname + '/preload.js'
    },
    frame: false
  })
  win.setResizable(false);
  isDev && win.webContents.openDevTools()



  ioHook.on('mousedown', (event) => {
    console.log(event); // { type: 'mousemove', x: 700, y: 400 }
    mouseDownPos = event
  });

  ioHook.on('mouseup', (event) => {
    console.log(event); // { type: 'mousemove', x: 700, y: 400 }
    mouseUpPos = event;
    savePrint(mouseDownPos, mouseUpPos)
    
  });

  win.loadURL(
    isDev
      ? 'http://localhost:3000'
      : `file://${path.join(__dirname, '../build/index.html')}`
  )
}

// PRINT -> Cria a tela transparente por cima de tudo para poder clicar e arrastar 
function drawPrint() {
  var screenSize = robot.getScreenSize();
  mouseDownPos = null
  mouseUpPos= null

  clipboard.clear();

  ioHook.start();

  printArea = new BrowserWindow({
    width: screenSize.width,
    height: screenSize.height,
    frame: false,
    transparent: true,
  })
  win.setPosition(screenSize.width, screenSize.height);

  globalShortcut.register("Esc", () => {
    removeWinPrint()
  });
}

async function savePrint(down, up){

  console.log(down.x, down.y, up.x - down.x, up.y - down.y)
  var img = await robot.screen.capture(down.x, down.y, up.x - down.x, up.y - down.y);
  screenCaptureToClipboard(img)
  
}

function screenCaptureToClipboard(robotScreenPic) {
  return new Promise((resolve, reject) => {
      try {
          const image = new Jimp(robotScreenPic.width, robotScreenPic.height);
          let pos = 0;
          image.scan(0, 0, image.bitmap.width, image.bitmap.height, (x, y, idx) => {
              image.bitmap.data[idx + 2] = robotScreenPic.image.readUInt8(pos++);
              image.bitmap.data[idx + 1] = robotScreenPic.image.readUInt8(pos++);
              image.bitmap.data[idx + 0] = robotScreenPic.image.readUInt8(pos++);
              image.bitmap.data[idx + 3] = robotScreenPic.image.readUInt8(pos++);
          });
          //image.write('./teste.png', resolve);
          console.log('Imagem copiada');
          var nimage = nativeImage.createFromBitmap(image.bitmap.data, {width:robotScreenPic.width, height:robotScreenPic.height})

          clipboard.writeImage(nimage)
          removeWinPrint();
          resolve()
      } catch (e) {
          console.error(e);
          reject(e);
      }
  });
}


function removeWinPrint(){
  ioHook.unregisterAllShortcuts();
  ioHook.stop();

  if(printArea){
    printArea.close()
    printArea = null;
  }else{
    printArea = null;
  }
  console.log('esc')
  globalShortcut.unregister('Esc')
  if (process.platform === 'darwin') {
    app.hide ();
    }else{
      win.hide ()
    }
  
}

// Remove o regostro das teclas 
function unRegisterKey (){
 // Unregister a shortcut.
 globalShortcut.unregister('CommandOrControl+;')
 globalShortcut.unregister('Esc')
  
 // Unregister all shortcuts.
 globalShortcut.unregisterAll()
}

// Move o mouse para a posição indicada
function moveMouse (pos){
    robot.setMouseDelay(2);
    setTimeout(() => {robot.moveMouseSmooth(pos[0], pos[1])}, 1000);
    setTimeout(() => {robot.moveMouseSmooth(pos[2], pos[3])}, 2000);
}

// Vem do React -> Ativa o Print
ipcMain.on('print', async (event, arg) => {
  drawPrint ()
});

// Vem do React -> Move o mouse
ipcMain.on('movemouse', async (event, arg) => {
    moveMouse (arg)
  });

  // Vem do React -> Posta os codigos do clipboard
  ipcMain.on('setHint', async (event, arg) => {
    app.hide ()
    for(var i=0; i<arg.length; i++){
        setTimeout(postCodes, 500 * i, arg[i], arg.length>0?true:false);
    }
 
  });

  // Vem do React -> Pega dados do clipboard e retorna pro react
  ipcMain.on('getClipboard', async (event, arg) => {
    const text = clipboard.readText()
    event.reply('getClipboard-reply', text)
  });

  // Posta os codigos no form
  function postCodes(code, tab){
    clipboard.writeText(code)

    setTimeout(() => {
        robot.keyTap('v', process.platform==='darwin' ? 'command' : 'control')
    }, 150)


    tab && setTimeout(() => {
        robot.keyTap('tab')
    }, 300)
  }

  // Init da classe
app.on('ready', createWindow)

// Classe ativa registra os shortcuts
app.whenReady().then(() => {
    const ret = globalShortcut.register('CommandOrControl+;', () => {
      if(win.isVisible()){
        if (process.platform === 'darwin') {
          app.hide ();
          }else{
            win.hide ()
          }
      }else{
        var mouse = robot.getMousePos();
        win.setPosition(mouse.x+200, mouse.y-200)
        win.show()
        win.webContents.send('setFocusInput', '')
      }
     
    })
  
    if (!ret) {
      console.log('registration failed')
    }
  
    // Check whether a shortcut is registered.
    console.log(globalShortcut.isRegistered('CommandOrControl+;'))

    
  })

  app.on('will-quit', () => {
    unRegisterKey()
  })

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  unRegisterKey()
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

// Listener para MAC
app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (process.platform === 'darwin') {
    app.dock.hide ();
  }
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// Esconder o APP caso clique fora da janela
  app.on('browser-window-blur', (event, win) => {
    if (win.webContents.isDevToolsFocused()) {
    } else {
      if(!printArea){
        
        if (process.platform === 'darwin') {
          app.hide ();
        }else{
          win.hide ()
        }
      }
    }
  })

  