import { useState, useEffect, useRef } from 'react'
import { CodeInput } from './CodeInput'
import { ShowInfo } from './ShowInfo'
import { ListViewer } from './ListViewer'
const xml2js = require('xml2js');
//const { ipcRenderer } = window.require('electron');

const fs = window.require('fs')
const pathModule = window.require('path')

const { app } = window.require('@electron/remote')

function App() {
  const ref = useRef();
  const injectCode = useRef();
  const infoCode = useRef();
  const [state, setState] = useState({path:app.getAppPath(), items:null})
  const {path, items} = state

  useEffect(() => {
    function read() {
      let values = [];
      var parser = new xml2js.Parser();
      
      const dataPath = process.env.NODE_ENV === 'development'?'/hints.xml':'../hints.xml';
  
      fs.readFile(pathModule.join(path, dataPath), function(err, data) {
          parser.parseString(data, function (err, result) {
  
              for(let i=0; i<result.hints.command.length; i++){
                values.push({id:i, name:result.hints.command[i].$.name, code:result.hints.command[i].code, info:result.hints.command[i].$.info, type:result.hints.command[i].$.type?result.hints.command[i].$.type:''})
              }
              setState(prevState => ({
                ...prevState,
                items:values
              }));
          });
      });
        }
    read();

    window.ipcRenderer.on('getClipboard-reply', (event, arg) => {
      injectCode.current.clipReceived(arg)
    })

    window.ipcRenderer.on('setFocusInput', (event, message) => { 
      ref?.current?.focus()
    });

    return () => {}
  }, [path])

  async function moveMouse(pos) {
    const res = await window.ipcRenderer.send('movemouse', pos);
    return res;
  }

  async function printScr() {
    const res = await window.ipcRenderer.send('print');
    return res;
  }

  

  const setHint = async (text) => {
    await window.ipcRenderer.send('setHint', text);
  }

  const getClipboard = async () => {
    await window.ipcRenderer.send('getClipboard');
  }

  const [searchString, setSearchString] = useState('')
  const filteredFiles = items?.filter(s => s.name.indexOf(searchString) !== -1)

  return (
    <div style={{display:'flex', flexDirection:'row'}}>
      <div style={{width:'30%'}}>
      <div style={{position: "fixed", top:0, left:0, backgroundColor:'white', width:'30%', height:90, zIndex:1}}>
        <div className="form-group mt-4 mb-2" style={{
              position: "fixed",
              top: 0,
              left: 20,
              right: 20,
            }}>
          <input
            ref={ref}
            value={searchString}
            onChange={event => setSearchString(event.target.value)}
            className="form-control form-control-sm"
            placeholder="Procurar..."
            style={{width:'37%'}}
          />
          <p className="text-black-50" style={{fontSize:12}}>
          
              Use as setas para selecionar as opções
           
          </p>
        </div>
      </div>
      <div style={{width:185, marginTop:90, marginLeft:20, marginBottom:20}} >
        <ListViewer items={filteredFiles} setId={injectCode} setInfo={infoCode}/>
      </div>
      </div>
      <div style={{width:'70%'}}>
        <CodeInput ref={injectCode} items={items} setHint={setHint} getClipboard={getClipboard} moveMouse={moveMouse} printScr={printScr}/>
        <ShowInfo ref={infoCode} items={items}/>
      </div>
      
    </div>
  )
}

export default App