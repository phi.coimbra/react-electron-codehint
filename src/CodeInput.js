import {
  useState,
  forwardRef,
  useImperativeHandle,
  useEffect,
} from "react";

export const CodeInput = forwardRef(({ items, setHint, getClipboard, moveMouse, printScr }, ref) => {
  const [state, setState] = useState({
    code: null,
    inputs: [],
    indices:[],
    refs: {},
    type: "",
    loop:null
  });
  const { code, inputs, type, indices, loop } = state;

  useImperativeHandle(ref, () => ({
    setId(id) {
      const item = items.filter((s) => s.id === id);

      setState((prevState) => ({
        ...prevState,
        code: item[0].code,
        type: item[0].type,
      }));

      setDefaultCodes();
    },
    sendHint() {
      if (type === "sort") {
        getClipboard();
      } else if(type === "mouse"){
          if(!loop){
            moveM();
            let id = setInterval(moveM, 1000 * 60);
            setState((prevState) => ({
                ...prevState,
                loop: id,
              }));
          }else{
            clearInterval(loop)
            setState((prevState) => ({
                ...prevState,
                loop: null,
              }));
          }
      }else if(type === "print"){
        printScr()
      }else{
        setHint(codeToPaste());
      }
    },
    clipReceived(text) {
      let orgArray = text.split("\n");

      cleanLines(orgArray);
    },
  }));

  const moveM = () => {
      console.log('move')
    moveMouse(inputs)
  }

  const cleanLines = (lines) => {
      let newLines = []
    for (let i = 0; i < lines.length; i++) {
      for (let a = 0; a < inputs.length; a++) {
        if (lines[i].indexOf(inputs[a]) !== -1) {
            newLines[i] = lines[i].substr(
            lines[i].indexOf(inputs[a]),
            lines[i].length
          )
        }
      }
    }
    //console.log(newLines);
    orderLines(newLines);
  };

  const orderLines = (lines) => {
    let result = [];

  // find preferences and put them into result
  inputs.forEach((line) => {
    result = result.concat(
        lines.filter((d) => d.indexOf(line) !== -1)
    );
  });

  for(let i=0; i<indices.length; i++){
     // console.log(inputs[i], indices[i], result.indexOf(inputs[i]))
    
    for(let a=0; a<result.length; a++){
        if(result[a].indexOf(inputs[i]) !== -1){
            result.splice(a, 0, indices[i]);
            break
        }
    }
  }

  let refactor = []
  refactor.push(result.join('\r\n'))

  //return result.concat(rest);
  setHint(refactor);

   
  };

  useEffect(() => {
    code && setDefaultCodes();
    return () => {};
    // eslint-disable-next-line
  }, [code]);

  const setDefaultCodes = () => {
    let myInputs = [];
    let indices = []
    let number = -1;
    code?.map((item, i) => {
      let parts = item.split(/\{([^}]+)\}/g);
      for (var a = 1; a < parts.length; a += 2) {
        number++;
        myInputs[number] = parts[a];
      }
      for (var b = 0; b < parts.length; b += 2) {
        number++;
        indices[number] = parts[b]
      }
      return true
    });
    let cleanArray = indices.filter(n => n);
    //console.log(cleanArray)
    setState((prevState) => ({
      ...prevState,
      inputs: myInputs,
      indices:cleanArray
    }));
  };

  const saveString = (texto, i) => {
    let myInputs = [...inputs];
    myInputs[i] = texto;

    setState((prevState) => ({
      ...prevState,
      inputs: myInputs,
    }));
  };

  const getInput = (number, placeholder) => {
    return (
      <input
        key={number}
        onChange={(event) => saveString(event.target.value, number)}
        className="form-control form-control-sm"
        placeholder={placeholder}
        style={{ width: 100, marginTop: 2, marginLeft: 5, marginRight: 5 }}
      />
    );
  };

  const getAllStuff = () => {
    let number = -1;
    return code?.map((item, i) => {
      let parts = item.split(/\{([^}]+)\}/g);
      //console.log(parts)
      for (var a = 1; a < parts.length; a += 2) {
        number++;
        parts[a] = getInput(number, parts[a]);
      }
      return (
        <li key={i}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
              alignItems: "center",
              width:500
            }}
          >
            {parts}
          </div>
        </li>
      );
    });
  };

  const codeToPaste = () => {
    //console.log('enter')
    let codes = [];
    let number = -1;
    for (let a = 0; a < code.length; a++) {
      let parts = code[a].split(/\{([^}]+)\}/g);

      for (let i = 1; i < parts.length; i += 2) {
        number++;
        parts[i] = inputs[number];
      }
      //console.log(parts.toString())
      codes.push(parts.toString().split(",").join(""));
    }
    return codes;
  };

  return (
    <div
      style={{
        position: "fixed",
        top: 25,
        left: "40%",
        backgroundColor: "white",
        width: "100%",
      }}
    >
      {code && (
        <div style={{ flexWrap: "wrap", alignItems: "center" }}>
          <ul>{getAllStuff()}</ul>
        </div>
      )}
    </div>
  );
});
