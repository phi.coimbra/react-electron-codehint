import React, { useState, useEffect } from "react";

const useKeyPress = function (targetKey) {
  const [keyPressed, setKeyPressed] = useState(false);

  function downHandler({ key }) {
    if (key === targetKey) {
      setKeyPressed(true);
    }
  }

  const upHandler = ({ key }) => {
    if (key === targetKey) {
      setKeyPressed(false);
    }
  };

  React.useEffect(() => {
    window.addEventListener("keydown", downHandler);
    window.addEventListener("keyup", upHandler);

    return () => {
      window.removeEventListener("keydown", downHandler);
      window.removeEventListener("keyup", upHandler);
    };
  });

  return keyPressed;
};

export const ListViewer = ({ items, setId, setInfo }) => {
  const [selected, setSelected] = useState(false);
  const downPress = useKeyPress("ArrowDown");
  const upPress = useKeyPress("ArrowUp");
  const enterPress = useKeyPress("Enter");
  const [cursor, setCursor] = useState(0);

  const refs = items?.reduce((acc, value) => {
    // console.log(acc, value)
    acc[value.id] = React.createRef();
    return acc;
  }, {});

  const ListItem = ({ item, active }) => (
    <li
      className={!active ? "list-group-item" : (selected && item.type === 'mouse'? "list-group-item list-group-item-success":"list-group-item active")}
      ref={refs[item.id]}
      onClick={() => {}}
      onMouseEnter={() => {}}
      onMouseLeave={() => {}}
    >
      {item.name}
    </li>
  );

  const moveScroll = (move) => {
    if (move >= 0 && move < items.length) {
      refs[move].current.scrollIntoView({
        behavior: "smooth",
        block: "center",
      });
      setChosed(move);
    }
  };

  const setChosed = (id) => {
    setId.current.setId(id);
    setInfo.current.setInfo(id);
  };

  useEffect(() => {
    items?.length>0 && setChosed(items[0].id);
    return () => {};
    // eslint-disable-next-line
  }, [items]);

  useEffect(() => {
    if (items?.length && downPress) {
      const newCursor =
        cursor < items.length - 1 ? items[cursor + 1].id : items[cursor];

      setCursor(cursor < items.length - 1 ? cursor + 1 : cursor);
      moveScroll(newCursor);
    }
    // eslint-disable-next-line
  }, [downPress]);
  useEffect(() => {
    if (items?.length && upPress) {
      setCursor((cursor) => (cursor > 0 ? cursor - 1 : cursor));
      moveScroll(cursor > 0 ? items[cursor - 1].id : items[cursor]);
    }
    // eslint-disable-next-line
  }, [upPress]);
  useEffect(() => {
    if (items?.length && enterPress) {
      //console.log('press', enterPress);
      //console.log('items acessados', items[cursor])
      
      if(items[cursor].type === 'mouse'){
        setSelected(!selected);
      }
      setId.current.sendHint(/*items[cursor].name*/);
    }
    // eslint-disable-next-line
  }, [enterPress]);

  return (
    <ul className="list-group list-group-flush">
      {items?.map((item, i) => (
        <ListItem
          key={item.id}
          active={i === cursor}
          item={item}
          setSelected={setSelected}
        />
      ))}
    </ul>
  );
};
