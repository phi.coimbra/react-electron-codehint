import { forwardRef, useImperativeHandle, useState } from 'react';

// eslint-disable-next-line no-empty-pattern
export const ShowInfo = forwardRef(({items}, ref) => {

    const [state, setState] = useState({info:''})
    const {info} = state
    
    useImperativeHandle(
        ref,
        () => ({
            setInfo(id) {

               const item = items.filter(s => s.id === id)

               setState(prevState => ({
                   ...prevState,
                   info:item[0].info
               }))
            },
            
        }),
    )

    return <div className="alert alert-primary" role="alert" style={{fontSize:14, position: "fixed", bottom:5, left:'40%', backgroundColor:'white', width:'100%', height:30, paddingLeft:10, paddingTop:2}}>{info}</div>
})